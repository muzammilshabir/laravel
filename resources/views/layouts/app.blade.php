<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
   <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <link rel="icon" type="image/ico" href="favicon.ico">
       
    <title>Laravel</title>
	<?php
	// $base_url = {{URL::to("/")}};
	 ?>
        <link rel="stylesheet" href="{{URL::to("/")}}/public/assests/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{URL::to("/")}}/public/assests/css/jqx.base.css">
        <link rel="stylesheet" href="{{URL::to("/")}}/public/assests/css/menu.css">
        
        <!-- Common JS -->
        <!-- jQuery framework -->
        <script src="{{URL::to("/")}}/public/assests/js/jquery-1.11.1.min.js"></script>
        <!-- top menu -->
        <script src="{{URL::to("/")}}/public/assests/js/jquery.fademenu.js"></script>
        <!-- top mobile menu -->
        <script src="{{URL::to("/")}}/public/assests/js/selectnav.min.js"></script>
        <!-- common functions -->
        <script src="{{URL::to("/")}}/public/assests/js/menu.js"></script>
        
        <!-- jqx widjets --> 
        <script src="{{URL::to("/")}}/public/assests/js/jqx-all.js"></script>
        <script src="{{URL::to("/")}}/public/assests/js/jqxcore.js"></script>
   
   </head>
<body>
    <div class="main-wrapper">
        <br>
    	@yield('content')
	
        <!-- top bar -->
        @include('elements.menu')
        
    </div>
</body>
</html>
