<div class="navbar navbar-fixed-top">
                <div class="navbar-inner">
                    <div class="container">
                        <div id="fade-menu" class="pull-left"  style="width: 100%">
                            <ul class="clearfix" id="mobile-nav">
                                <li>
                                    <a href="javascript:void(0)">Settings</a>
                                    <ul>
                                    
                                        <li>
                                    		 <a href={{URL::to('/')}}"/settings">Settings</a>
                                        </li>
                                        <li>
                                            <a href="index463f.html?page=form_validation">Form validation</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Components</a>
                                    <ul>
                                        <li>
                                            <a href="indexb320.html?page=calendar">Calendar</a>
                                        </li>
                                        <li>
                                            <a href="indexb730.html?page=charts">Charts</a>
                                        </li>
                                        <li>
                                            <a href="indexb300.html?page=contact_list">Contact List</a>
                                        </li>
                                        <li>
                                            <a href="index6f93.html?page=datatables">Datatables</a>
                                        </li>
                                        <li>
                                            <a href="indexd586.html?page=editable_elements">Editable Elements</a>
                                        </li>
                                        <li>
                                            <a href="index5f4e.html?page=file_manager">File manager</a>
                                        </li>
                                        <li>
                                            <a href="indexd590.html?page=gallery">Gallery</a>
                                        </li>
                                        <li>
                                            <a href="indexae6f.html?page=gmaps">Google Maps</a>
                                        </li>
                                        <li>
                                            <a href="#">Tables</a>
                                            <ul>
                                                <li><a href="index8a93.html?page=tables_regular">Regular Tables</a></li>
                                                <li><a href="index1088.html?page=table_stacking">Stacking Table</a></li>
                                                <li><a href="index19e1.html?page=table_examples">Table examples</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="indexbb4d.html?page=wizard">Wizard</a>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">UI Elements</a>
                                    <ul>
                                        <li><a href="index9d8e.html?page=alerts_buttons">Alerts, Buttons</a></li>
                                        <li><a href="index7632.html?page=grid">Grid</a></li>
                                        <li><a href="index7fa9.html?page=icons">Icons</a></li>
                                        <li><a href="index8067.html?page=js_grid">JS Grid</a></li>
                                        <li>
                                            <a href="index7d5f.html?page=notifications">Notifications</a>
                                        </li>
                                        <li><a href="index7a1c.html?page=tabs_accordions">Tabs, Accordions</a></li>
                                        <li><a href="index2043.html?page=tooltips_popovers">Tooltips, Popovers</a></li>
                                        <li><a href="indexa62f.html?page=typography">Typography</a></li>
                                        <li><a href="index589c.html?page=widgets">Widgets</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Other pages</a>
                                    <ul>
                                        <li><a href="index72d4.html?page=ajax_content">Ajax content</a></li>
                                        <li><a href="index2154.html?page=blank">Blank page</a></li>
                                        <li><a href="index1b62.html?page=blog_page">Blog page</a></li>
                                        <li><a href="index9852.html?page=blog_page_single">Blog page (single)</a></li>
                                        <li><a href="index38c4.html?page=chat">Chat</a></li>
                                        <li><a href="error_404.html">Error 404</a></li>
                                        <li><a href="indexbb2d.html?page=help_faq">Help/Faq</a></li>
                                        <li><a href="indexb59b.html?page=invoices">Invoices</a></li>
                                        <li><a href="login.html">Login Page</a></li>
                                        <li><a href="indexa8a8.html?page=mailbox">Mailbox</a></li>
                                        <li><a href="index0aa3.html?page=user_profile">User profile</a></li>
                                        <li><a href="index74a7.html?page=settings">Site Settings</a></li>
                                    </ul>
                                </li>
                                <li><a href="javascript:void(0)">Sub-menu</a>
                                    <ul>
                                        <li><a href="#">Section 1</a></li>
                                        <li><a href="#">Section 2</a></li>
                                        <li><a href="#">Section 3</a></li>
                                        <li>
                                            <a href="#">Section 4</a>
                                            <ul>
                                                <li><a href="#">Section 4.1</a></li>
                                                <li><a href="#">Section 4.2</a></li>
                                                <li><a href="#">Section 4.3</a></li>
                                                <li>
                                                    <a href="#">Section 4.4</a>
                                                    <ul>
                                                        <li><a href="#">Section 4.4.1</a></li>
                                                        <li><a href="#">Section 4.4.2</a></li>
                                                        <li><a href="#">Section 4.4.4</a></li>
                                                        <li><a href="#">Section 4.4.5</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Section5</a></li>
                                        <li><a href="#">Section6</a></li>
                                    </ul>
                                </li>
                                 
                                <li style="float: right; ">
                                    <a href="javascript:void(0)">welcome</a>
                                    <ul>
                                    <div class="col-md-4" style="float: right; margin-top: 3px; display: block; border: solid 1px; height: 200px;  background-color: graytext">
                            <div class="user-box">
                                <div class="user-box-inner">
                                    <img src="img/avatars/avatar.png" alt="" class="user-avatar img-avatar">
                                    <div class="user-info">
                                        Welcome, <strong>Jonathan</strong>
                                        <a href="index0aa3.html?page=user_profile">Settings</a>
                                            <a href="login.html">Logout</a>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                                        
                                    </ul>
                                </li>
                                 
                             
                            </ul>
                               
                        </div>
                    </div>
                </div>
            </div>